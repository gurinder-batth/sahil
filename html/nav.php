<h1 class="title">
      {PHP} <small class="">Login/Register</small>
    </h1>
    <p class="subtitle">
      <div class="tabs is-centered">
        <ul>
       <?php if(isset($_SESSION['user'])) { ?>
           <li>
            <a href="<?= html_url('index') ?>">
              <span class="icon is-small"><i class="fas fa-home" aria-hidden="true"></i></span>
              <span>Home</span>
            </a>
          </li>
        <?php } else { ?>
          <li>
            <a href="<?= html_url('login') ?>">
              <span class="icon is-small"><i class="fas fa-sign" aria-hidden="true"></i></span>
              <span>Login</span>
            </a>
          </li>
          <li>
            <a href="<?= html_url('signup') ?>">
              <span class="icon is-small"><i class="fas fa-user" aria-hidden="true"></i></span>
              <span>Register</span>
            </a>
          </li>
        <?php } ?>
        </ul>
      </div>
    </p>