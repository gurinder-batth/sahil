<div class="field is-horizontal">
    <div class="field-label is-normal">
        <label class="label">Username</label>
    </div>
    <div class="field-body">
        <div class="field">
            <p class="control">
                <input class="input" type="email">
            </p>
        </div>
    </div>
</div>

<div class="field is-horizontal">
    <div class="field-label is-normal">
        <label class="label">Password</label>
    </div>
    <div class="field-body">
        <div class="field">
            <p class="control">
                <input class="input" type="password" placeholder="Password">
            </p>
        </div>
    </div>
</div>

<div class="field is-horizontal">
    <div class="field-label is-normal">
    </div>
    <div class="field-body">
        <div class="field">
            <p class="control">
                <button class="button is-primary">Register</button>
            </p>
        </div>
    </div>
</div>